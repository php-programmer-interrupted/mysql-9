-- Проверка домашнего задания 
use shop; 

-- С помощью команды UPDATE заполнить alias_name для всех категорий 
UPDATE category set alias_name = 'women''s clothing' WHERE id = 1; 
UPDATE category set alias_name = 'man''s clothing' WHERE id = 2; 
UPDATE category set alias_name = 'women''s shoes' WHERE id = 3; 

-- Добавить новый бренд «Тетя Клава Company» 
INSERT INTO category (name, discount) VALUES ('Тетя Клава Company', 0); 

-- Удалить этот бренд 
DELETE FROM category WHERE id = 5;